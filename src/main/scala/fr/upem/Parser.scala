package fr.upem

import java.io.{FileNotFoundException, IOException}

import scala.io.Source
import scala.util.Try

case class Parser(lines : String){
  val board = Parser.patternFirst.findFirstIn(lines)
  val mowers = Parser.patternMower.findAllIn(lines)
}


object Parser {

  val patternFirst = "([0-9]+ [0-9]+)".r
  val coordsWithDirection = "([0-9]+ [0-9]+ [NEWS])"
  val moveCommands = "([AGD]+)"
  val number = "[0-9]+"
  val direction = "[NEWS]"

  val patternCoordsWithDirection = coordsWithDirection.r
  val patternMoveCommands = moveCommands.r

  val patternMower = (coordsWithDirection + "\n" + moveCommands).r
  val patternNumber = number.r
  val patternDirection = direction.r

  def parse(fileName : String): Try[String] ={
    Try {
      val lines = Source.fromFile(fileName)
      val parsed = lines.getLines().mkString("\n");
      parsed
    }


    /*
    val lines = Source.fromFile(fileName)
      if (lines.isEmpty) {
        println("Wrong file")
        System.exit(0)
      }
    lines.getLines().mkString("\n");

    */
  }


}
