package fr.upem

import fr.upem.Command._
import fr.upem.Direction.{East, North, South, West}


case class Mower (direction : Direction, position: Position){

  def forward() : Mower = direction match {
    case North => {
      val p = Position(position.x, position.y +1)
      this.copy(position = p)
    }
    case South => {
      val p = Position(position.x, position.y -1)
      this.copy(position = p)
    }
    case East =>{
      val p = Position(position.x + 1, position.y)
      this.copy(position = p)
    }
    case West =>{
      val p = Position(position.x - 1 , position.y)
      this.copy(position = p)
    }
  }



  def move(command : Command): Mower = command match{
    case Forward => forward()
    case Left => this.copy(direction = direction.left)
    case Right => this.copy(direction = direction.right)
  }

  def canMove(maxSize : Position): Boolean ={
    position.x >= 0 && position.y >= 0 && position.y <= maxSize.y && position.x <= maxSize.x
  }
}



