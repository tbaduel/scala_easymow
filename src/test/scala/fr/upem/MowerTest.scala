package fr.upem

import fr.upem.Command.Forward
import fr.upem.Command.Left
import fr.upem.Command.Right
import fr.upem.Direction.North
import fr.upem.Direction.East
import fr.upem.Direction.South
import fr.upem.Direction.West
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class MowerTest extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks {

  "Mower" should "move forward to the right direction" in {
    Mower(North, Position(1,1)).move(Forward) should be (Mower(North, Position(1,2)))
    Mower(East, Position(1,1)).move(Forward) should be (Mower(East, Position(2,1)))
    Mower(South, Position(1,1)).move(Forward) should be (Mower(South, Position(1,0)))
    Mower(West, Position(1,1)).move(Forward) should be (Mower(West, Position(0,1)))
  }

  "Mower" should "execute right command" in {
    Mower(North, Position(1,1)).move(Left) should be (Mower(West, Position(1,1)))
    Mower(East, Position(1,1)).move(Left) should be (Mower(North, Position(1,1)))
    Mower(South, Position(1,1)).move(Left) should be (Mower(East, Position(1,1)))
    Mower(West, Position(1,1)).move(Left) should be (Mower(South, Position(1,1)))

    Mower(North, Position(1,1)).move(Right) should be (Mower(East, Position(1,1)))
    Mower(East, Position(1,1)).move(Right) should be (Mower(South, Position(1,1)))
    Mower(South, Position(1,1)).move(Right) should be (Mower(West, Position(1,1)))
    Mower(West, Position(1,1)).move(Right) should be (Mower(North, Position(1,1)))
  }

  "Mower" should "can move in map" in {
    Mower(North,Position(1,1)).canMove(Position(4,4)) should be(true)
    Mower(North,Position(4,4)).canMove(Position(4,4)) should be(true)
    Mower(North,Position(4,5)).canMove(Position(4,4)) should be(false)
    Mower(North,Position(4,4)).canMove(Position(3,4)) should be(false)
    Mower(South,Position(-1,0)).canMove(Position(4,4)) should be(false)
  }

}
