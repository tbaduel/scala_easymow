package fr.upem

import java.nio.file.NoSuchFileException

import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FlatSpec, Matchers}

import scala.util.{Failure, Success}

class ParserTest extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks {

  "Parser" should "not return an error if file exist" in {
    val file = Parser.parse("file.txt")
    val res = file match {
      case Success(str) => "good file"
      case Failure(e) => "Wrong file"
    }
    res should be {"good file"}
  }

  "Parser" should "return a Failure if file doesn't exist" in {
    val file = Parser.parse("wrong.txt")
    val res = file match {
      case Success(str) => "good file"
      case Failure(e) => "Wrong file"
    }
    res should be {"Wrong file"}
  }

}
