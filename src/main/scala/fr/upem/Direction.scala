package fr.upem

import fr.upem.Direction.{East, North, South, West}


object StartDirection{
  def getDirection(direction : String) : Direction ={
    direction match {
      case "N" => North
      case "S" => South
      case "W" => West
      case "E" => East
    }
  }
}

sealed trait Direction{
  def left: Direction
  def right : Direction
}

object Direction {

  case object North extends Direction {
    override def left = West
    override def right = East

  }
  case object East extends Direction{
    override def left = North
    override def right = South
  }
  case object South extends Direction{
    override def left = East
    override def right = West
  }
  case object West extends Direction{
    override def left = South
    override def right = North
  }
}
