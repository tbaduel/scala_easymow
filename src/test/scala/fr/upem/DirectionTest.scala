package fr.upem

import org.scalatest.{FlatSpec, Matchers}

class DirectionTest extends FlatSpec with Matchers {

  "Direction" should "be in the right direction when turn left" in {
    Direction.North.left should be(Direction.West)
    Direction.West.left should be(Direction.South)
    Direction.South.left should be(Direction.East)
    Direction.East.left should be(Direction.North)
  }

  "Direction" should "be in the right direction when turn right" in {
    Direction.North.right should be(Direction.East)
    Direction.West.right should be(Direction.North)
    Direction.South.right should be(Direction.West)
    Direction.East.right should be(Direction.South)
  }

  "Direction" should "be the right direction when asking with string" in {
    StartDirection.getDirection("N") should be (Direction.North)
    StartDirection.getDirection("W") should be (Direction.West)
    StartDirection.getDirection("S") should be (Direction.South)
    StartDirection.getDirection("E") should be (Direction.East)
  }

}
