package fr.upem

import fr.upem.Command.{Forward, Left, Right}


object Commands{
  def getCommand(command : String): Command ={
    command match {
      case "A" => Forward
      case "G" => Left
      case "D" => Right
    }
  }
}


sealed trait Command

object Command {
  case object Forward extends Command
  case object Left extends Command
  case object Right extends Command

}
