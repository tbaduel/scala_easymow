
# Comment lancer le programme :

Il faut le lancer avec la commande sbt habituelle
 
```sbt run```

Le résultat du programme s'affiche dans le terminal.

__Attention__ à bien avoir le fichier ___```File.txt```___ dans le dossier racine du projet.


Pour lancer les test : 
```sbt test```

# Difficultés rencontrées

Ecrire en fonctionnel a été le plus difficile, n'étant pas habitué et n'ayant pas eut des
résultats exceptionnel dans les matières correspondant à ces langages.

De plus le système pour parser une ligne puis deux lignes à la fois n'est pas habituel

# Détails d'implémentation

### Parseur

Le parseur a été réalisé en plusieurs fois. La première itération faisait ligne a ligne, mais dans la
partie ou il fallait récupérer les coordonnées initiales de la tondeuse ainsi que ses 
movements, le ligne à ligne n'était plus utilisable.

J'ai alors changé l'implémentation

### La tondeuse 

La tondeuse a été modélisée de la façon suivante :
*  Elle contient :
    * la direction dans laquelle elle se déplace
    * les coordonnées en x et en y de la case sur laquelle elle est
* La direction ainsi que la position sont des objets, permettant de faciliter leur utilisation
