
import fr.upem._

import scala.util.{Failure, Success}

object Main extends App {
  val file = "./file.txt"

  val textFile = Parser.parse(file)

  textFile match{
    case Success(parsed)=>
      val initCoordsString = Parser.patternNumber.findAllIn(Parser(parsed).board.get)
      val map = Position(initCoordsString.next().toInt, initCoordsString.next().toInt)


      Parser(parsed).mowers.foreach(x => {
        val tmp = Parser.patternCoordsWithDirection.findFirstIn(x)
        val coords = Parser.patternNumber.findAllIn(tmp.get)
        val direction = Parser.patternDirection.findFirstIn(tmp.get)
        val position = Position(coords.next().toInt, coords.next().toInt)

        val moves = Parser.patternMoveCommands.findFirstIn(x).get

        val mower = Mower(StartDirection.getDirection(direction.get), position)

        val end = moves.foldLeft(mower) {
          case (mower, c) => {
            val nextMower = mower.move(Commands.getCommand(c.toString));
            //println(mower.move(Commands.getCommand(c.toString)));
            if (nextMower.canMove(map)) nextMower else mower
          }
        }

        println(end.position.x + " " + end.position.y + " " + end.direction.toString)
      })

    case Failure(e) => println("Wrong file")
  }
  
}
